# Atkinson Dither 
Atkinson Dithering is a [style of 1-bit greyscale dithering](https://en.wikipedia.org/wiki/Dither#Algorithms) invented by Bill Atkinson, an early Apple engineer. The effect characterized the early "Apple aesthetic" although at the time it was born of need. 

## API


## Web UI
This package also presents a drag-and-drop web UI

## Credit

Dithering code:
Adapted from [node-atkinson-dither](https://github.com/rtrvrtg/node-atkinson-dither) by [rtrvrtg](https://github.com/rtrvrtg)

Zip:
<https://github.com/Touffy/client-zip>

Lightbox:
<https://fslightbox.com/>


K8:
```yaml
apiVersion: v1
kind: Service
metadata:
  name: dither
spec:
  ports:
    - port: 80
      targetPort: 8888
  selector:
    app: dither
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dither
spec:
  replicas: 1
  selector:
    matchLabels:
      app: dither
  strategy: {}
  template:
    metadata:
      labels:
        app: dither
    spec:
      containers:
        - name: dither
          image: registry.gitlab.com/anfaux/dither
          imagePullPolicy: Always
          resources: {}
          ports:
            - containerPort: 8888
      restartPolicy: Always
      serviceAccountName: ''
status: {}
```
