const canvas = require("canvas");
const atkinsonDither = require("./atkinson-dither");
const fs = require("fs");
const path = require("path");
const { v4: uuidv4 } = require("uuid");
const os = require("os");
const tempDir = `${os.tmpdir()}/atkinson`;

const atkinson = (inputFile, cb) => {
  const outputFile = `${tempDir}/${uuidv4()}.${
    inputFile.split(".")[inputFile.split(".").length - 1]
  }`;
  fs.readFile(inputFile, (err, data) => {
    if (err) {
      cb(err, false);
      return;
    }
    const img = new canvas.Image();
    img.onload = function () {
      const c = canvas.createCanvas(img.width, img.height),
        ctx = c.getContext("2d");
      ctx.drawImage(img, 0, 0, img.width, img.height);
      const imgData = ctx.getImageData(0, 0, img.width, img.height);
      const processed = atkinsonDither.ProcessImage(imgData);
      ctx.putImageData(processed, 0, 0);
      const buf = c.toBuffer();
      fs.writeFile(outputFile, buf, function (err, res) {
        if (!!err) {
          cb(err, false);
        } else {
          cb(outputFile, true);
        }
      });
    };
    img.src = data;
  });
};

module.exports = atkinson;
