const fs = require("fs");
const path = require("path");
const express = require("express");
const app = express();
const port = 8888;
const enableUi = true;
const formidable = require("formidable");
const atkinsonDither = require(`./modules/atkinson`);
const os = require("os");
const { v4: uuidv4 } = require("uuid");
const MagickCLI = require("magick-cli");
const { exec } = require("child_process");

const makeDir = (dir, cb) => {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
    if (typeof cb === "function") cb();
  } else {
    if (typeof cb === "function") cb();
  }
};

const tempDir = `${os.tmpdir()}/atkinson`;
makeDir(tempDir);

if (enableUi) {
  app.use(express.static(path.join(__dirname, "www")));
}

const applyMagick = (inputFile, recipe) => {
  const outputFile = `${uuidv4()}.${
    inputFile.split(".")[inputFile.split(".").length - 1]
  }`;
  MagickCLI.executeSync(`/usr/bin/ ${inputFile} ${recipe} ${outputFile}`);
  fs.renameSync(outputFile, inputFile);
  return inputFile;
};

const processWithOptions = (req, res, process) => {
  const resizeBefore = parseFloat(req.query.rb) || null;
  const resizeAfter = parseFloat(req.query.ra) || null;
  new formidable.IncomingForm().parse(req, (err, fields, files) => {
    if (Object.entries(files).length === 0) res.sendStatus(400);
    if (err) {
      console.error("Error", err);
      throw err;
      res.sendStatus(400);
    }
    for (let object of Object.entries(files)) {
      const file = object[1];
      console.log(`- ${file.originalFilename} | ${file.size}`);
      let inputFile = `${tempDir}/${file.originalFilename}`;
      fs.renameSync(file.filepath, inputFile);
      if (resizeBefore) applyMagick(inputFile, `-resize ${resizeBefore}%`);
      process({
        inputFile,
        sendResult: (file) => {
          if (resizeAfter) applyMagick(file, `-resize ${resizeAfter}%`);
          res.sendFile(file, () => {
            fs.unlink(file, (err) => {
              if (err) throw err;
            });
          });
        }
      });
    }
  });
};

app.post("/halftone", (req, res) => {
  processWithOptions(req, res, ({ inputFile, sendResult }) => {
    // Color Halftone
    // https://legacy.imagemagick.org/discourse-server/viewtopic.php?t=35494
    let recipe = "";
    recipe += "-set option:distort:viewport %wx%h+0+0 ";
    recipe += "-colorspace CMYK ";
    recipe += "-separate null: ( -size 3x3 xc: ( +clone -negate ) ";
    recipe += "+append ( +clone -negate ) -append ) ";
    recipe += "-virtual-pixel tile ";
    recipe += "-filter gaussian ( +clone -distort SRT 60 ) ";
    recipe += "+swap ( +clone -distort SRT 30 ) ";
    recipe += "+swap ( +clone -distort SRT 45 ) ";
    recipe += "+swap ( +clone -fill black -colorize 100 -distort SRT 0 ) ";
    recipe += "+swap +delete -compose Overlay  ";
    recipe += "-layers composite  ";
    recipe += "-set colorspace CMYK  ";
    recipe += "-combine -colorspace RGB";
    applyMagick(inputFile, recipe);
    sendResult(inputFile);
  });
});

app.post("/atkinson", (req, res) => {
  processWithOptions(req, res, ({ inputFile, sendResult }) => {
    atkinsonDither(inputFile, (ditheredFile) => {
      sendResult(ditheredFile);
    });
  });
});

app.listen(port, () => {
  console.log("-------------------------------");
  console.log(`Dither listening on port ${port}`);
  console.log("-------------------------------");
});
