let uploadProgress = [];
let dropArea;
let inProcessCounter = 0;
let overlayTimer;
let counter = 0;

const downloadBlob = (blob, name) => {
  const blobUrl = URL.createObjectURL(blob);
  const link = document.createElement("a");
  link.href = blobUrl;
  link.download = name;
  document.body.appendChild(link);
  link.dispatchEvent(
    new MouseEvent("click", {
      bubbles: true,
      cancelable: true,
      view: window
    })
  );
  document.body.removeChild(link);
};

function preventDefaults(e) {
  e.preventDefault();
  e.stopPropagation();
}

function handleDrop(e) {
  var dt = e.dataTransfer;
  var files = dt.files;
  handleFiles(files);
}
function initializeProgress(numFiles) {
  const progressBar = document.getElementById("progress-bar");
  progressBar.value = 0;
  uploadProgress = [];

  for (let i = numFiles; i > 0; i--) {
    uploadProgress.push(0);
  }
}

function clearAll() {
  document.getElementById("download-all").style.display = "none";
  document.getElementById("clear-all").style.display = "none";
  document.getElementById("gallery").innerHTML = "";
}

async function downloadAll() {
  const files = [];
  const downloadables = document.getElementsByClassName("downloadable");

  for (let el of downloadables) {
    let blobUrl = el.getElementsByTagName("a")[0].href;
    let fileName = el.getElementsByTagName("img")[0].dataset.name;

    let file = await fetch(blobUrl)
      .then((r) => r.blob())
      .then(
        (blobFile) => new File([blobFile], fileName, { type: "image/png" })
      );
    files.push(file);
  }
  const blob = await downloadZip(files).blob();
  const link = document.createElement("a");
  link.href = URL.createObjectURL(blob);
  link.download = "atkinson.zip";
  link.click();
  link.remove();
  URL.revokeObjectURL(link.href);
}
function updateProgress(fileNumber, percent) {
  const progressBar = document.getElementById("progress-bar");
  uploadProgress[fileNumber] = percent;
  let total =
    uploadProgress.reduce((tot, curr) => tot + curr, 0) / uploadProgress.length;
  progressBar.style.opacity = total > 0 && total < 100 ? 1 : 0;
  progressBar.value = total;
  document.getElementById("working-overlay").style.display = "flex";
  if (total === 100) {
    setTimeout(() => {
      refreshFsLightbox();
      document.getElementsByTagName("html")[0].classList = "";
      document.getElementById("download-all").style.display = "inline-block";
      document.getElementById("clear-all").style.display = "inline-block";
    }, 250);
  }
}

function previewFile(id, file) {
  let reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onloadend = function () {
    let img = document.createElement("img");
    img.src = reader.result;
    img.id = `preview_${id}`;
    img.dataset.name = file.name;

    let div = document.createElement("div");
    div.className = "loading";
    div.appendChild(img);

    document.getElementById("gallery").appendChild(div);
  };
}

function uploadFile(count, id, file) {
  const progressBar = document.getElementById("progress-bar");
  progressBar.style.opacity = 1;
  document.getElementById("working-overlay").style.display = "flex";
  inProcessCounter++;
  var url = "atkinson";
  var xhr = new XMLHttpRequest();
  var formData = new FormData();

  xhr.open("POST", url, true);
  xhr.responseType = "blob";
  xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
  xhr.upload.addEventListener("progress", function (e) {
    updateProgress(count, (e.loaded * 100.0) / e.total || 100);
  });
  xhr.addEventListener("readystatechange", function (e) {
    if (xhr.readyState == 4 && xhr.status == 200) {
      updateProgress(count, 100);
      var blob = this.response;

      const imageEl = document.getElementById(`preview_${id}`);
      imageEl.class = "downloadable";
      const parentDiv = imageEl.parentElement;

      imageEl.remove();
      imageEl.onload = () => {
        inProcessCounter--;
        if (inProcessCounter === 0)
          setTimeout(() => {
            document.getElementById("working-overlay").style.display = "none";
          }, 250);
      };
      imageEl.src = window.URL.createObjectURL(blob);
      const linkEl = document.createElement("a");
      linkEl.href = window.URL.createObjectURL(blob);
      linkEl.dataset.fslightbox = "gallery";
      linkEl.dataset.name = file.name;
      linkEl.appendChild(imageEl);

      parentDiv.className = "downloadable";
      const downloadButton = document.createElement("div");
      downloadButton.classList = "button downloadButton";
      downloadButton.innerText = "⇩";
      downloadButton.addEventListener("click", () =>
        downloadBlob(blob, file.name)
      );
      parentDiv.setInnerHtml = "";
      parentDiv.appendChild(downloadButton);
      parentDiv.appendChild(linkEl);
    } else if (xhr.readyState == 4 && xhr.status != 200) {
      console.error("Error");
    }
  });

  formData.append("id", id);
  formData.append("file", file);
  xhr.send(formData);
}

function handleFiles(files) {
  files = [...files];
  console.log(files);

  files = files.filter(
    (file) =>
      file.type.includes("image") && !file.type.includes("vnd.adobe.photoshop")
  );
  initializeProgress(files.length);
  files.forEach((file, idx) => uploadFile(idx, counter + idx, file));
  files.forEach((file, idx) => previewFile(counter + idx, file));
  counter += files.length;
}

window.addEventListener("load", () => {
  dropArea = document.getElementById("drop-area");
  ["dragenter", "dragover", "dragleave", "drop"].forEach((eventName) => {
    dropArea.addEventListener(eventName, preventDefaults, false);
    document.body.addEventListener(eventName, preventDefaults, false);
  });
  ["dragenter", "dragover"].forEach((eventName) => {
    dropArea.addEventListener(
      eventName,
      () => dropArea.classList.add("active"),
      false
    );
  });
  ["dragleave", "drop"].forEach((eventName) => {
    dropArea.addEventListener(
      eventName,
      () => dropArea.classList.remove("active"),
      false
    );
  });
  dropArea.addEventListener("drop", handleDrop, false);
  uploadProgress = [];
});
